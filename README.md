# Gitpod Examples for the Kubernetes Agent

Use it with the GitLab Kubernetes agent examples in this repository: https://gitlab.com/everyonecancontribute/kubernetes/k8s-agent 

```
gitops:
  manifest_projects:
  - id: "everyonecancontribute/kubernetes/gitops-examples"
    paths:
    - glob: '/**/*.{yaml,yml,json}'
```

